package main

import (
	"calculator/handler"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"calculator/config"
)

var f *os.File // logFile handle

func init() {
	logFile := config.ReadLogFileConfig()
	file, err := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	f = file
	//set output of logs to f
	log.SetOutput(f)
}

func main() {
	defer end()

	log.Println("API in port 8080")

	router := mux.NewRouter()

	router.HandleFunc("/health", handler.Health).Methods("GET")
	router.HandleFunc("/calculator", handler.Home).Methods("GET")
	router.HandleFunc("/calculator/add/{data}", handler.Addition).Methods("GET", "POST")
	router.HandleFunc("/calculator/sub/{data}", handler.Subtraction).Methods("GET", "POST")
	router.HandleFunc("/calculator/mlt/{data}", handler.Multiplication).Methods("GET", "POST")
	router.HandleFunc("/calculator/div/{data}", handler.Division).Methods("GET", "POST")

	log.Println(http.ListenAndServe(":8080", router))
}

func end() {
	if f != nil {
		defer f.Close()
	}
}
