package handler

import (
	"log"
	"net/http"
)

//Health is a simple health check
func Health(w http.ResponseWriter, _ *http.Request) {
	log.Println("Health invoked")

	//w.WriteHeader(http.StatusOK)
	w.Header().Set("content-type", "application/json")

	w.Write([]byte("{\"Alive\": true}"))
}

// Home shows the way how to use this calc
func Home(w http.ResponseWriter, _ *http.Request) {
	log.Println("Home invoked")

	w.Header().Set("content-type", "application/json")

	w.Write([]byte("{\"Select an operation\" : {\"addition\":\"add\", \"subtraction\": \"sub\", \"multiplication\": \"mlt\", \"division\": \"div\"}," +
		" \"GET request format\": \"/calculator/add/1,2,3\"," +
		" \"POST request format\":  {\"body\":\"JSON array\", \"url\": \"/calculator/add/post\"}}"))
}