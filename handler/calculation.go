package handler

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

var (
	myErr = []byte("{\"Error\": \"Invalid data\"}")
	divOnZero = []byte("{\"Error\": \"Invalid operation\"}")
)

// JSONResponse creates response in JSON format
func JSONResponse(w http.ResponseWriter, status []byte) {
	w.Header().Set("content-type", "application/json")
	w.Write(status)
}

// Convert data from []string to []float64
func convertData(data []string) ([]float64, error) {
	var cnvData []float64
	for _, value := range data {
		a, err := strconv.ParseFloat(value, 64)
		if err != nil {
			return cnvData, err
		}
		cnvData = append(cnvData, a)
	}
	return cnvData, nil
}

// Handler for data in GET request
func requestGETHandler(r *http.Request) ([]float64, error) {
	vars := mux.Vars(r)
	data := strings.Split(vars["data"], ",")
	fData, err := convertData(data)
	return fData, err
}

// Handler fo data in POST request
func requestPOSTHandler(r *http.Request) ([]float64, error){
	defer r.Body.Close()
	var data []float64
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return data, err
	}
	if errData := json.Unmarshal(body, &data); errData != nil{
		return data, errData
	}
	return data, nil
}

// Inspect the method of request
func inspectMethod(r *http.Request) ([]float64, error) {
	switch r.Method {
	case "GET":
		data, err := requestGETHandler(r)
		return data, err
	case "POST":
		data, err := requestPOSTHandler(r)
		return data, err
	}
	return nil, nil
}

// Addition calculate the sum of elements
func Addition(w http.ResponseWriter, r *http.Request) {
	log.Println("Addition invoked")

	data, err := inspectMethod(r)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		JSONResponse(w, myErr)
		return
	}
	result := data[0]
	for _, value := range data[1:] {
		result += value
	}
	answer, _ := json.Marshal(result)
	JSONResponse(w, answer)
}

// Subtraction calculate the difference of elements
func Subtraction(w http.ResponseWriter, r *http.Request) {
	log.Println("Substraction invoked")

	data, err := inspectMethod(r)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		JSONResponse(w, myErr)
		return
	}
	result := data[0]
	for _, value := range data[1:] {
		result = result - value
	}
	answer, _ := json.Marshal(result)
	JSONResponse(w, answer)
}

// Multiplication calculate the product of elements
func Multiplication(w http.ResponseWriter, r *http.Request) {
	log.Println("Multiplication invoked")

	data, err := inspectMethod(r)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		JSONResponse(w, myErr)
		return
	}
	result := data[0]
	for _, value := range data[1:] {
		result *= value
	}
	answer, _ := json.Marshal(result)
	JSONResponse(w, answer)
}

// Division calculate the ratios of elements
func Division(w http.ResponseWriter, r *http.Request) {
	log.Println("Division invoked")

	data, err := inspectMethod(r)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		JSONResponse(w, myErr)
		return
	}
	result := data[0]
	for _, value := range data[1:] {
		if value == 0{
			JSONResponse(w, divOnZero)
			return
		}
		result = result / value
	}
	answer, _ := json.Marshal(result)
	JSONResponse(w, answer)
}
